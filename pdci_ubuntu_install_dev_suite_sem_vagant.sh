#!/usr/bin/env bash
rm -rf ./.env
set -e
PDCI_APPLICATION_ENV=development
usage()
{
    echo "======================================================================================="
    echo "Selecione em qual ambiente ira instalar a DEV Suite:                                   "
    echo ""
    echo "      -dev  | dev                Ambiente de Desenvolvimento (Programador)"
    echo "======================================================================================="
}

export_env()
{
rm -rf export.env
cat .env | while read line
do
    echo "export $line" >> export.env
done
export
echo ""
echo ""
cat export.env
echo ""
echo ""
source export.env
export

echo ""
}

cp env.dev .env
export_env

sudo sed -i "s|ExecStart=/usr/bin/dockerd -H fd://|ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H fd://|g" /lib/systemd/system/docker.service


systemctl daemon-reload
systemctl restart docker

__pdci_docker_host=$(cat .env | grep pdci_docker_host_dev |  sed -e "s|pdci_docker_host_dev=||g" )
__pdci_application_env=$(cat .env | grep PDCI_APPLICATION_ENV |  sed -e "s|PDCI_APPLICATION_ENV=||g" )
__pdci_docker_host_ip=$(cat .env | grep pdci_docker_host_dev |  sed -e "s|pdci_docker_host_dev=||g" |  sed -e "s|tcp://||g" |  sed -e "s|:2375||g" )


echo "__pdci_docker_host => ${__pdci_docker_host}"
echo "__pdci_application_env=> ${__pdci_application_env}"
if [ "${__pdci_docker_host}" == "" ]; then

    echo "ERRO :: Ambiente nao foi informado"

    exit 1
fi

verifica_instalacao_docker()
{
    existe=$(which docker)

if [ "${existe}" == "" ]; then
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh
    rm -rf get-docker.sh
fi

    existe=$(which docker)

if [ "${existe}" == "" ]; then
    echo "ERRO :: DOCKER NAO ESTA INSTALADO"
    exit 1
fi

existe=$(which docker-compose)

if [ "${existe}" == "" ]; then
    curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
fi

existe=$(which docker-compose)

if [ "${existe}" == "" ]; then
  echo "ERRO :: DOCKER COMPOSER NAO ESTA INSTALADO"
  exit 1
fi

}

verifica_instalacao_docker

echo "" >> .env #importante que tenha esta linha para que seja adicionado a proxima linha no arquivo .env

#echo "Clonando o projeto https://gitlab.com/pdci/pdci_jenkins_dood_alpine"
#rm -rf pdci_jenkins_dood_alpine
#git clone https://gitlab+deploy-token-51823:TbCakWCLg4NBBAkPqM8C@gitlab.com/pdci/pdci_jenkins_dood_alpine
#cd pdci_jenkins_dood_alpine
#cd dev-dood-build
#docker -H ${__pdci_docker_host}  image build --build-arg DOCKER_GROUP_ID=$(getent group docker | awk -F: '{printf "%d\n", $3}') -t pdci/jenkins-dood-build:latest . --no-cache
#cd ..
#cd ..
echo ""
echo ""
export
echo ""
echo ""
docker -H ${__pdci_docker_host}  container prune -f
echo ""
echo ""
docker -H ${__pdci_docker_host}  image prune -f
echo ""
echo ""
if [ "${__pdci_application_env}" != "" ]; then
    echo "__pdci_application_env =>  ${__pdci_application_env} "
else
    echo "__pdci_application_env =>  ${__pdci_application_env} "
    echo "Favor informar o PDCI_APPLICATION_ENV=development  (${__pdci_application_env}) no arquivo env.dev"
    exit 1
fi

if [ "${__pdci_docker_host}" != "" ]; then
    echo "Docker Host ${__pdci_docker_host} "
else
    echo "ERRO :: Favor informar o Docker Host em __pdci_docker_host (${__pdci_docker_host}) no arquivo env.dev"
    exit 1
fi
echo ""
echo ""
docker -H ${__pdci_docker_host} login -u gitlab+deploy-token-51966 -p iGNHJ25ZTufvWX8BTKbv registry.gitlab.com
echo ""
docker -H ${__pdci_docker_host} container prune -f
echo ""
echo ""
docker  -H ${__pdci_docker_host}  network create pdci_dev_suite_${__pdci_application_env}_network || true
echo ""
echo ""
# TODO FAZER COM QUE O SCRIPT DESCUBRA QUAIS SAO OS SERVICOS QUE VEEM DO REGISTRY E APENAS NESTES REALIZE O PULL
docker-compose  -H ${__pdci_docker_host}  -p pdci_dev_suite_${__pdci_application_env} -f  docker-compose-dood.yml  pull
echo ""
echo "Removendo container jenkins_dood_dev_alpine ..."
echo ""
docker-compose  -H ${__pdci_docker_host} -p pdci_dev_suite_${__pdci_application_env} -f  docker-compose-dood.yml rm -f -s jenkins_dood_dev_alpine
echo ""
echo ""
echo "Removendo volume pdci_dev_suite_${__pdci_application_env}_jenkins_dood_dev_alpine_home"
echo ""
docker  -H ${__pdci_docker_host} volume remove pdci_dev_suite_${__pdci_application_env}_jenkins_dood_dev_alpine_home || true
echo ""
echo ""
echo "Criando volume pdci_dev_suite_${__pdci_application_env}_jenkins_dood_dev_alpine_home"
echo ""
echo ""
docker  -H ${__pdci_docker_host}  volume create pdci_dev_suite_${__pdci_application_env}_jenkins_dood_dev_alpine_home || true
echo ""
echo "docker-compose  -H ${__pdci_docker_host}    -p pdci_dev_suite_${__pdci_application_env} -f docker-compose-dood.yml config"
docker-compose  -H ${__pdci_docker_host}  -p pdci_dev_suite_${__pdci_application_env} -f docker-compose-dood.yml config
echo ""
echo ""
docker-compose  -H ${__pdci_docker_host}  -p pdci_dev_suite_${__pdci_application_env} -f docker-compose-dood.yml  up -d
rm -f export.env
echo ""
echo ""
cat .env
#rm -f .env
echo ""
echo ""
docker-compose  -H ${__pdci_docker_host}  -p pdci_dev_suite_${__pdci_application_env} -f docker-compose-dood.yml  ps

## Verificar se os servicos subiram?
rm -f lista_servicos
echo ""
echo ""

docker-compose  -H ${__pdci_docker_host}  -p pdci_dev_suite_${__pdci_application_env} -f docker-compose-dood.yml  ps | awk -F " " ' { print $1 }' | sed "1d" | sed "1d" > lista_servicos
container_port=""
cat lista_servicos | while read line
do
    echo " Serviço => $line"
    container_id=$(docker -H ${__pdci_docker_host} ps | grep $line | awk -F " " ' { print $1 }')
    if [ -n ${container_id} ]; then
    echo " docker -H ${__pdci_docker_host} ps | grep pdci_jenkins_dood_alpine  | awk -F ":" ' { print $3 }' | awk -F "->" ' { print $1 }'"
        container_image=$(docker -H ${__pdci_docker_host} ps | grep $line | awk -F " " ' { print $2 }')
        container_error=$(docker -H ${__pdci_docker_host} container inspect  --format='{{.State.Error}}' ${container_id})
        container_port=$( docker -H ${__pdci_docker_host} ps | grep pdci_jenkins_dood_alpine  | awk -F ":" ' { print $3 }' | awk -F "->" ' { print $1 }')
        echo " -H __pdci_docker_host = ${__pdci_docker_host}"
        echo "   container_id => ${container_id}"
        echo "   container_image => ${container_image}"
        echo "   container_error => ${container_error}"
    else
      echo "ERRO :: O conteiner do servico $line nao subio. Verifique o problema"
    fi

    echo ""
done
echo "============================================================"
echo ""
echo "    Abra o jenkins do dev => http://${__pdci_docker_host_ip}:41085    "
echo ""
echo "============================================================"

