# Utilizar o jenkins DEV diretamente na maquina UBUNTU do Desenvolvedor
Para Instalar o jenkins na própria máquina Ubuntu do desenvolvedor  sem a utilização do vagrant e nem do docker host no Centos 7

Informe o ip da sua máquina no arquivo env.dev no parametro `"pdci_docker_host_dev=tcp://192.168.56.101:2375" `

Altere a permissão do arquivo pdci_ubuntu_install_dev_suite_sem_vagant.sh para executavel

`chmod +x pdci_ubuntu_install_dev_suite_sem_vagant.sh`

execute como root o arquivo `./pdci_ubuntu_install_dev_suite_sem_vagant.sh `

# dev-dockerhost-co7-pdci
O Vagrant é uma ferramenta para criar e gerenciar ambientes de máquinas virtuais em um único fluxo de trabalho. 
O PDCI ira utilizar o vagrant para criar uma máquina virtual de centos 7 no virtual box.
Assim que a VM do virtual box estiver pronta o vagrant irá instalar o DOCKER HOST de Desenvolvimento. 
Este Ambiente será chmado de DEV. 
Será subido o container do jenkins otimizados para os trabalhos dos desenvolvedores dentro do PDCI. 
Assim que o ambiente estiver pronto o desenvolvedor poderar acessar o jenkins pelo endereco http://192.168.56.133:41085

## Instalar o Vargrant
Siga as intruções de instalação em https://www.vagrantup.com/docs/installation/

## Projeto de automação do ambiente pdci_dev_suite com vagrant.

Clone o projeto pdci_dev_suite em https://gitlab.com/pdci/dev-dockerhost-co7-pdci.git

dentro da pasta pdci_dev_suite digite o seguinte commando:

`vagrant plugin install vagrant-hostmanager`

`vagrant plugin install vagrant-vbguest`

`vagrant plugin install nugrant`

`vagrant up`




usuário na VM: vagrant
senha: vagrant

## Criacao de projto no gitlab

TODO



## Acessando a Suite de desenvolvimento do PDCI.

http://192.168.56.133:41085

