#!/usr/bin/env bash
#set -e
echo "========================================================================="
echo "                      Instalando Docker e containerd.io                  "
echo "========================================================================="
echo ""
sudo apt-get install curl apt-utils  -y
sudo curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
#echo "========================================================================="
#echo "                     Adicionar atual nao-root no grupo root            "
#echo "========================================================================="
sudo usermod -aG docker $(whoami)
echo ""
echo "========================================================================="
echo "                      Habilitando acesso pelo api                        "
echo "========================================================================="
echo ""
sudo sed -i "s|ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock|ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H fd:// -H unix://var/run/docker.sock --containerd=/run/containerd/containerd.sock|g" /lib/systemd/system/docker.service
echo ""
sudo systemctl daemon-reload
sudo systemctl enable docker
sudo systemctl restart docker
docker -v
echo ""
echo "========================================================================="
echo "                      Setando Timezone                 "
echo "========================================================================="
echo ""
sudo timedatectl set-timezone America/Sao_Paulo
echo ""
if [ ! -e /usr/bin/docker-compose ]; then
   echo ""
   echo "========================================================================="
   echo "                      Install Docker Compose                             "
   echo "========================================================================="
   echo ""
    sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
    sudo docker-compose --version
   echo ""
fi
#       echo "========================================================================="
#       echo "                      Atualizando SO                                     "
#       echo "========================================================================="
#       echo ""
#       yum  --setopt=tsflags=nodocs  update -y
#       echo ""
#echo "========================================================================="
#echo ""
#echo "                      Download do PDCI SUITE DEV                         "
#echo "                Suite de apoio do PDCI para Desenvolvedores              "
#echo ""
#echo "========================================================================="
#echo ""
#rm -rf pdci_dev_suite
#git clone https://gitlab+deploy-token-51665:SDJ3G7At8VTzTgML1Ps9@gitlab.com/pdci/pdci_dev_suite.git
#cd pdci_dev_suite
#sed -i "s|pdci_docker_host_dev=tcp://192.168.56.133:2375|pdci_docker_host_dev=tcp://127.0.0.1:2375|g" env.dev
#chmod +x pdci_install_dev_suite.sh
#./pdci_install_dev_suite.sh dev
#cd ..
#echo ""
#echo "========================================================================="
#echo ""
#echo ""
#echo ""
#echo "       Abra o jenkins do dev => http://IP_DA_MAQUINA:41085"
#echo ""
#echo "========================================================================="
#echo ""